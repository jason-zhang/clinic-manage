execute sp_revokedbaccess DBO
go

alter table DBO.MEDICINE_INFO_SPELL_INDEX
   drop constraint FK_M_I_R_M_I_S_I
go

alter table DBO.PRESCRIPTION_DETAIL
   drop constraint FK_P_D_R_M_I
go

alter table DBO.PRESCRIPTION_DETAIL
   drop constraint FK_P_D_R_P_I
go

alter table DBO.PRESCRIPTION_INFO
   drop constraint FK_P_I_D_U_N
go

alter table DBO.PRESCRIPTION_INFO
   drop constraint FK_P_I_R_C_U_I
go

alter table DBO.PRESCRIPTION_INFO_SPELL_INDEX
   drop constraint FK_P_I_S_I_R_P_I
go

alter table DBO.STOCK_INFO
   drop constraint FK_S_I_R_M_I
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.V_STOCK_INFO')
            and   type = 'V')
   drop view DBO.V_STOCK_INFO
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_BAR_CODE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_BAR_CODE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_MEDICINE_INFO_STATE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_MEDICINE_INFO_STATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_MEDICINE_MANUFACTURER'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_MEDICINE_MANUFACTURER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_MEDICINE_NAME_MANUFACTURER'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_MEDICINE_NAME_MANUFACTURER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_MEDICINE_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_MEDICINE_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO')
            and   name  = 'IDX_M_I_TIME_STAMP'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO.IDX_M_I_TIME_STAMP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO_SPELL_INDEX')
            and   name  = 'IDX_M_I_S_I_MEDICINE_ID'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO_SPELL_INDEX.IDX_M_I_S_I_MEDICINE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.MEDICINE_INFO_SPELL_INDEX')
            and   name  = 'IDX_M_I_S_I_TIME_STAMP'
            and   indid > 0
            and   indid < 255)
   drop index DBO.MEDICINE_INFO_SPELL_INDEX.IDX_M_I_S_I_TIME_STAMP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_DETAIL')
            and   name  = 'IDX_DETAIL_MEDICINE_ID'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_DETAIL.IDX_DETAIL_MEDICINE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_DETAIL')
            and   name  = 'IDX_DETAIL_PRESCRIPTION_ID'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_DETAIL.IDX_DETAIL_PRESCRIPTION_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_AGE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_AGE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_CREATE_DATE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_CREATE_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_GENDER'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_GENDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_PATIENT'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_PATIENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_PRESCRIPTION_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_PRESCRIPTION_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_P_I_C_U_N'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_P_I_C_U_N
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_P_I_D_U_N'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_P_I_D_U_N
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_P_I_STATE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_P_I_STATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_P_I_S_N'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_P_I_S_N
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO')
            and   name  = 'IDX_P_I_TIME_STAMP'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO.IDX_P_I_TIME_STAMP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO_SPELL_INDEX')
            and   name  = 'IDX_P_I_S_I_PRESCRIPTION_ID'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO_SPELL_INDEX.IDX_P_I_S_I_PRESCRIPTION_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.PRESCRIPTION_INFO_SPELL_INDEX')
            and   name  = 'IDX_P_I_S_I_TIME_STAMP'
            and   indid > 0
            and   indid < 255)
   drop index DBO.PRESCRIPTION_INFO_SPELL_INDEX.IDX_P_I_S_I_TIME_STAMP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.SPELL')
            and   name  = 'CHINESE_CHARACTER'
            and   indid > 0
            and   indid < 255)
   drop index DBO.SPELL.CHINESE_CHARACTER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.SPELL')
            and   name  = 'IDX_SPELL'
            and   indid > 0
            and   indid < 255)
   drop index DBO.SPELL.IDX_SPELL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.STOCK_INFO')
            and   name  = 'IDX_STOCK_INFO_MEDICINE_ID'
            and   indid > 0
            and   indid < 255)
   drop index DBO.STOCK_INFO.IDX_STOCK_INFO_MEDICINE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.STOCK_INFO')
            and   name  = 'IDX_STORE_DATE'
            and   indid > 0
            and   indid < 255)
   drop index DBO.STOCK_INFO.IDX_STORE_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DBO.USER_INFO')
            and   name  = 'IDX_USER_INFO_L_S_R_T'
            and   indid > 0
            and   indid < 255)
   drop index DBO.USER_INFO.IDX_USER_INFO_L_S_R_T
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.MEDICINE_INFO')
            and   type = 'U')
   drop table DBO.MEDICINE_INFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.MEDICINE_INFO_SPELL_INDEX')
            and   type = 'U')
   drop table DBO.MEDICINE_INFO_SPELL_INDEX
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.PRESCRIPTION_DETAIL')
            and   type = 'U')
   drop table DBO.PRESCRIPTION_DETAIL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.PRESCRIPTION_INFO')
            and   type = 'U')
   drop table DBO.PRESCRIPTION_INFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.PRESCRIPTION_INFO_SPELL_INDEX')
            and   type = 'U')
   drop table DBO.PRESCRIPTION_INFO_SPELL_INDEX
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.SPELL')
            and   type = 'U')
   drop table DBO.SPELL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.STOCK_INFO')
            and   type = 'U')
   drop table DBO.STOCK_INFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DBO.USER_INFO')
            and   type = 'U')
   drop table DBO.USER_INFO
go

/*==============================================================*/
/* User: DBO                                                    */
/*==============================================================*/
execute sp_grantdbaccess DBO
go

/*==============================================================*/
/* Table: MEDICINE_INFO                                         */
/*==============================================================*/
create table DBO.MEDICINE_INFO (
   MEDICINE_ID          int                  identity(1,1),
   BAR_CODE             varchar(13)          null,
   MEDICINE_NAME        nvarchar(128)        null,
   MANUFACTURER         nvarchar(128)        null,
   SPECIFICATION        nvarchar(64)         null,
   MEDICINE_TYPE        tinyint              null,
   UNIT1                nvarchar(32)         null,
   COST1                money                null,
   PRICE1               money                null,
   UNIT2                nvarchar(32)         null,
   COST2                money                null,
   PRICE2               money                null,
   UNIT3                nvarchar(32)         null,
   COST3                money                null,
   PRICE3               money                null,
   RATIO1               int                  null,
   RATIO2               int                  null,
   STOCK1               int                  null default 0,
   STOCK2               int                  null default 0,
   STOCK3               int                  null default 0,
   STATE                tinyint              null default 0,
   TIME_STAMP           timestamp            null,
   constraint PK_MEDICINE_INFO primary key (MEDICINE_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '药品标识',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'MEDICINE_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '条形码',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'BAR_CODE'
go

execute sp_addextendedproperty 'MS_Description', 
   '药品的通用名和商品名之间以空格或者括号进行分隔存入到药品名称中',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'MEDICINE_NAME'
go

execute sp_addextendedproperty 'MS_Description', 
   '生产企业',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'MANUFACTURER'
go

execute sp_addextendedproperty 'MS_Description', 
   '规格',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'SPECIFICATION'
go

execute sp_addextendedproperty 'MS_Description', 
   '0:药品  1:手术 2:器材',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'MEDICINE_TYPE'
go

execute sp_addextendedproperty 'MS_Description', 
   '最小销售单位',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'UNIT1'
go

execute sp_addextendedproperty 'MS_Description', 
   '最小单位进价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'COST1'
go

execute sp_addextendedproperty 'MS_Description', 
   '最小单位售价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'PRICE1'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间销售单位',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'UNIT2'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间单位进价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'COST2'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间单位售价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'PRICE2'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大销售单位',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'UNIT3'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大单位进价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'COST3'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大单位售价',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'PRICE3'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间-最小销售单位间的比率',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'RATIO1'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大-中间销售单位比率',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'RATIO2'
go

execute sp_addextendedproperty 'MS_Description', 
   '最小销售单位库存量',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'STOCK1'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间销售单位库存量',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'STOCK2'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大销售单位库存量',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'STOCK3'
go

execute sp_addextendedproperty 'MS_Description', 
   '0:有效 1:无效',
   'user', 'DBO', 'table', 'MEDICINE_INFO', 'column', 'STATE'
go

/*==============================================================*/
/* Index: IDX_BAR_CODE                                          */
/*==============================================================*/
create unique index IDX_BAR_CODE on DBO.MEDICINE_INFO (
BAR_CODE ASC
)
go

/*==============================================================*/
/* Index: IDX_MEDICINE_NAME_MANUFACTURER                        */
/*==============================================================*/
create index IDX_MEDICINE_NAME_MANUFACTURER on DBO.MEDICINE_INFO (
MEDICINE_NAME ASC,
MANUFACTURER ASC
)
go

/*==============================================================*/
/* Index: IDX_MEDICINE_MANUFACTURER                             */
/*==============================================================*/
create index IDX_MEDICINE_MANUFACTURER on DBO.MEDICINE_INFO (
MANUFACTURER ASC
)
go

/*==============================================================*/
/* Index: IDX_MEDICINE_TYPE                                     */
/*==============================================================*/
create index IDX_MEDICINE_TYPE on DBO.MEDICINE_INFO (
MEDICINE_TYPE ASC
)
go

/*==============================================================*/
/* Index: IDX_MEDICINE_INFO_STATE                               */
/*==============================================================*/
create index IDX_MEDICINE_INFO_STATE on DBO.MEDICINE_INFO (
STATE ASC
)
go

/*==============================================================*/
/* Index: IDX_M_I_TIME_STAMP                                    */
/*==============================================================*/
create index IDX_M_I_TIME_STAMP on DBO.MEDICINE_INFO (
TIME_STAMP ASC
)
go

/*==============================================================*/
/* Table: MEDICINE_INFO_SPELL_INDEX                             */
/*==============================================================*/
create table DBO.MEDICINE_INFO_SPELL_INDEX (
   INDEX_ID             int                  identity(1,1),
   MEDICINE_ID          int                  null,
   MEDICINE_NAME_SPELL  varchar(1024)        null,
   MANUFACTURER_SPELL   varchar(1024)        null,
   TIME_STAMP           timestamp            null,
   constraint PK_MEDICINE_INFO_SPELL_INDEX primary key (INDEX_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', 'DBO', 'table', 'MEDICINE_INFO_SPELL_INDEX', 'column', 'INDEX_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药品标识',
   'user', 'DBO', 'table', 'MEDICINE_INFO_SPELL_INDEX', 'column', 'MEDICINE_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药品名称拼音',
   'user', 'DBO', 'table', 'MEDICINE_INFO_SPELL_INDEX', 'column', 'MEDICINE_NAME_SPELL'
go

execute sp_addextendedproperty 'MS_Description', 
   '生产企业拼音',
   'user', 'DBO', 'table', 'MEDICINE_INFO_SPELL_INDEX', 'column', 'MANUFACTURER_SPELL'
go

/*==============================================================*/
/* Index: IDX_M_I_S_I_MEDICINE_ID                               */
/*==============================================================*/
create index IDX_M_I_S_I_MEDICINE_ID on DBO.MEDICINE_INFO_SPELL_INDEX (
MEDICINE_ID ASC
)
go

/*==============================================================*/
/* Index: IDX_M_I_S_I_TIME_STAMP                                */
/*==============================================================*/
create index IDX_M_I_S_I_TIME_STAMP on DBO.MEDICINE_INFO_SPELL_INDEX (
TIME_STAMP ASC
)
go

/*==============================================================*/
/* Table: PRESCRIPTION_DETAIL                                   */
/*==============================================================*/
create table DBO.PRESCRIPTION_DETAIL (
   DETAIL_ID            int                  identity(1,1),
   PRESCRIPTION_ID      int                  null,
   MEDICINE_ID          int                  null,
   UNIT                 nvarchar(32)         null,
   QUANTITY             int                  null,
   COST                 money                null,
   PRICE                money                null,
   constraint PK_PRESCRIPTION_DETAIL primary key (DETAIL_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '药单详细信息标识',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'DETAIL_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药单标识',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'PRESCRIPTION_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药品标识',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'MEDICINE_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '单位',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'UNIT'
go

execute sp_addextendedproperty 'MS_Description', 
   '数量',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'QUANTITY'
go

execute sp_addextendedproperty 'MS_Description', 
   '售价',
   'user', 'DBO', 'table', 'PRESCRIPTION_DETAIL', 'column', 'PRICE'
go

/*==============================================================*/
/* Index: IDX_DETAIL_PRESCRIPTION_ID                            */
/*==============================================================*/
create index IDX_DETAIL_PRESCRIPTION_ID on DBO.PRESCRIPTION_DETAIL (
PRESCRIPTION_ID ASC
)
go

/*==============================================================*/
/* Index: IDX_DETAIL_MEDICINE_ID                                */
/*==============================================================*/
create index IDX_DETAIL_MEDICINE_ID on DBO.PRESCRIPTION_DETAIL (
MEDICINE_ID ASC
)
go

/*==============================================================*/
/* Table: PRESCRIPTION_INFO                                     */
/*==============================================================*/
create table DBO.PRESCRIPTION_INFO (
   PRESCRIPTION_ID      int                  identity(1,1),
   PRESCRIPTION_TYPE    tinyint              null,
   PATIENT              nvarchar(50)         null,
   GENDER               nvarchar(1)          null,
   AGE                  int                  null,
   COST                 money                null,
   DUE                  money                null,
   RECEIPTS             money                null,
   PAID                 money                null,
   CREATE_DATE          datetime             null default GETDATE(),
   CREATE_USER_NAME     nvarchar(50)         null,
   DEAL_DATE            datetime             null,
   DEAL_USER_NAME       nvarchar(50)         null,
   STATE                tinyint              null default 0,
   SERIAL_NO            int                  null,
   TIME_STAMP           timestamp            null,
   constraint PK_PRESCRIPTION_INFO primary key (PRESCRIPTION_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '药单标识',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'PRESCRIPTION_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '0:处方药单 1:非处方药单 2:自用药单(费用为0)',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'PRESCRIPTION_TYPE'
go

execute sp_addextendedproperty 'MS_Description', 
   '病人姓名',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'PATIENT'
go

execute sp_addextendedproperty 'MS_Description', 
   '性别',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'GENDER'
go

execute sp_addextendedproperty 'MS_Description', 
   '年龄',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'AGE'
go

execute sp_addextendedproperty 'MS_Description', 
   '成本',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'COST'
go

execute sp_addextendedproperty 'MS_Description', 
   '应收',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'DUE'
go

execute sp_addextendedproperty 'MS_Description', 
   '实收',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'RECEIPTS'
go

execute sp_addextendedproperty 'MS_Description', 
   '实缴',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'PAID'
go

execute sp_addextendedproperty 'MS_Description', 
   '生成时间',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'CREATE_DATE'
go

execute sp_addextendedproperty 'MS_Description', 
   '生成用户',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'CREATE_USER_NAME'
go

execute sp_addextendedproperty 'MS_Description', 
   '处理用户',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'DEAL_USER_NAME'
go

execute sp_addextendedproperty 'MS_Description', 
   '0未处理 1已处理',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO', 'column', 'STATE'
go

/*==============================================================*/
/* Index: IDX_PRESCRIPTION_TYPE                                 */
/*==============================================================*/
create index IDX_PRESCRIPTION_TYPE on DBO.PRESCRIPTION_INFO (
PRESCRIPTION_TYPE ASC
)
go

/*==============================================================*/
/* Index: IDX_PATIENT                                           */
/*==============================================================*/
create index IDX_PATIENT on DBO.PRESCRIPTION_INFO (
PATIENT ASC
)
go

/*==============================================================*/
/* Index: IDX_GENDER                                            */
/*==============================================================*/
create index IDX_GENDER on DBO.PRESCRIPTION_INFO (
GENDER ASC
)
go

/*==============================================================*/
/* Index: IDX_AGE                                               */
/*==============================================================*/
create index IDX_AGE on DBO.PRESCRIPTION_INFO (
AGE ASC
)
go

/*==============================================================*/
/* Index: IDX_CREATE_DATE                                       */
/*==============================================================*/
create index IDX_CREATE_DATE on DBO.PRESCRIPTION_INFO (
CREATE_DATE ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_TIME_STAMP                                    */
/*==============================================================*/
create index IDX_P_I_TIME_STAMP on DBO.PRESCRIPTION_INFO (
TIME_STAMP ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_C_U_N                                         */
/*==============================================================*/
create index IDX_P_I_C_U_N on DBO.PRESCRIPTION_INFO (
CREATE_USER_NAME ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_D_U_N                                         */
/*==============================================================*/
create index IDX_P_I_D_U_N on DBO.PRESCRIPTION_INFO (
DEAL_USER_NAME ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_STATE                                         */
/*==============================================================*/
create index IDX_P_I_STATE on DBO.PRESCRIPTION_INFO (
STATE ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_S_N                                           */
/*==============================================================*/
create index IDX_P_I_S_N on DBO.PRESCRIPTION_INFO (
SERIAL_NO ASC
)
go

/*==============================================================*/
/* Table: PRESCRIPTION_INFO_SPELL_INDEX                         */
/*==============================================================*/
create table DBO.PRESCRIPTION_INFO_SPELL_INDEX (
   INDEX_ID             int                  identity(1,1),
   PRESCRIPTION_ID      int                  null,
   PATIENT_SPELL        varchar(1024)        null,
   TIME_STAMP           timestamp            null,
   constraint PK_PRESCRIPTION_INFO_SPELL_IND primary key (INDEX_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO_SPELL_INDEX', 'column', 'INDEX_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药单标识',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO_SPELL_INDEX', 'column', 'PRESCRIPTION_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '病人姓名拼音',
   'user', 'DBO', 'table', 'PRESCRIPTION_INFO_SPELL_INDEX', 'column', 'PATIENT_SPELL'
go

/*==============================================================*/
/* Index: IDX_P_I_S_I_PRESCRIPTION_ID                           */
/*==============================================================*/
create index IDX_P_I_S_I_PRESCRIPTION_ID on DBO.PRESCRIPTION_INFO_SPELL_INDEX (
PRESCRIPTION_ID ASC
)
go

/*==============================================================*/
/* Index: IDX_P_I_S_I_TIME_STAMP                                */
/*==============================================================*/
create index IDX_P_I_S_I_TIME_STAMP on DBO.PRESCRIPTION_INFO_SPELL_INDEX (
TIME_STAMP ASC
)
go

/*==============================================================*/
/* Table: SPELL                                                 */
/*==============================================================*/
create table DBO.SPELL (
   CHINESE_CHARACTER    nvarchar(1)          null,
   SPELL                varchar(64)          null
)
go

execute sp_addextendedproperty 'MS_Description', 
   '汉字',
   'user', 'DBO', 'table', 'SPELL', 'column', 'CHINESE_CHARACTER'
go

execute sp_addextendedproperty 'MS_Description', 
   '拼音',
   'user', 'DBO', 'table', 'SPELL', 'column', 'SPELL'
go

/*==============================================================*/
/* Index: CHINESE_CHARACTER                                     */
/*==============================================================*/
create index CHINESE_CHARACTER on DBO.SPELL (
CHINESE_CHARACTER ASC
)
go

/*==============================================================*/
/* Index: IDX_SPELL                                             */
/*==============================================================*/
create index IDX_SPELL on DBO.SPELL (
SPELL ASC
)
go

/*==============================================================*/
/* Table: STOCK_INFO                                            */
/*==============================================================*/
create table DBO.STOCK_INFO (
   STOCK_ID             int                  identity(1,1),
   MEDICINE_ID          int                  null,
   LOT_NUMBER           varchar(20)          null,
   MANUFACTURE_DATE     datetime             null,
   VALIDITY_DATE        datetime             null,
   STORE_DATE           datetime             null default GETDATE(),
   UNIT1_COUNT          int                  null,
   UNIT2_COUNT          int                  null,
   UNIT3_COUNT          int                  null,
   constraint PK_STOCK_INFO primary key (STOCK_ID)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '入库标识',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'STOCK_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '药品标识',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'MEDICINE_ID'
go

execute sp_addextendedproperty 'MS_Description', 
   '生产批号',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'LOT_NUMBER'
go

execute sp_addextendedproperty 'MS_Description', 
   '生产日期',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'MANUFACTURE_DATE'
go

execute sp_addextendedproperty 'MS_Description', 
   '有效日期',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'VALIDITY_DATE'
go

execute sp_addextendedproperty 'MS_Description', 
   '入库时间',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'STORE_DATE'
go

execute sp_addextendedproperty 'MS_Description', 
   '最小销售单位数量',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'UNIT1_COUNT'
go

execute sp_addextendedproperty 'MS_Description', 
   '中间销售单位数量',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'UNIT2_COUNT'
go

execute sp_addextendedproperty 'MS_Description', 
   '最大销售单位数量',
   'user', 'DBO', 'table', 'STOCK_INFO', 'column', 'UNIT3_COUNT'
go

/*==============================================================*/
/* Index: IDX_STOCK_INFO_MEDICINE_ID                            */
/*==============================================================*/
create index IDX_STOCK_INFO_MEDICINE_ID on DBO.STOCK_INFO (
MEDICINE_ID ASC
)
go

/*==============================================================*/
/* Index: IDX_STORE_DATE                                        */
/*==============================================================*/
create index IDX_STORE_DATE on DBO.STOCK_INFO (
STORE_DATE ASC
)
go

/*==============================================================*/
/* Table: USER_INFO                                             */
/*==============================================================*/
create table DBO.USER_INFO (
   USER_NAME            nvarchar(50)         not null,
   PASSWORD             varchar(32)          not null,
   USER_TYPE            tinyint              not null,
   LOGIN_DATE           datetime             null,
   ERROR_TIMES          int                  null,
   LOGIN_STATE          tinyint              null default 0,
   REPORT_TIME          datetime             null,
   constraint PK_USER_INFO primary key (USER_NAME)
)
go

execute sp_addextendedproperty 'MS_Description', 
   '用户名',
   'user', 'DBO', 'table', 'USER_INFO', 'column', 'USER_NAME'
go

execute sp_addextendedproperty 'MS_Description', 
   '密码',
   'user', 'DBO', 'table', 'USER_INFO', 'column', 'PASSWORD'
go

execute sp_addextendedproperty 'MS_Description', 
   '0:管理员 1:普通用户',
   'user', 'DBO', 'table', 'USER_INFO', 'column', 'USER_TYPE'
go

execute sp_addextendedproperty 'MS_Description', 
   '0:不在线 1:在线',
   'user', 'DBO', 'table', 'USER_INFO', 'column', 'LOGIN_STATE'
go

execute sp_addextendedproperty 'MS_Description', 
   '上报时间',
   'user', 'DBO', 'table', 'USER_INFO', 'column', 'REPORT_TIME'
go

/*==============================================================*/
/* Index: IDX_USER_INFO_L_S_R_T                                 */
/*==============================================================*/
create index IDX_USER_INFO_L_S_R_T on DBO.USER_INFO (
LOGIN_STATE ASC,
REPORT_TIME ASC
)
go

/*==============================================================*/
/* View: V_STOCK_INFO                                           */
/*==============================================================*/
create view DBO.V_STOCK_INFO as
select
   t1.MEDICINE_ID MEDICINE_ID,
   BAR_CODE,
   MEDICINE_NAME,
   MANUFACTURER,
   SPECIFICATION,
   MEDICINE_TYPE,
   UNIT1,
   COST1,
   PRICE1,
   UNIT2,
   COST2,
   PRICE2,
   UNIT3,
   COST3,
   PRICE3,
   RATIO1,
   RATIO2,
   STOCK1,
   STOCK2,
   STOCK3,
   STATE,
   STOCK_ID,
   LOT_NUMBER,
   MANUFACTURE_DATE,
   VALIDITY_DATE,
   STORE_DATE,
   UNIT1_COUNT,
   UNIT2_COUNT,
   UNIT3_COUNT
from
   MEDICINE_INFO t1,
   STOCK_INFO t2
where
   t1.MEDICINE_ID = t2.MEDICINE_ID
   and MEDICINE_TYPE = 0
go

alter table DBO.MEDICINE_INFO_SPELL_INDEX
   add constraint FK_M_I_R_M_I_S_I foreign key (MEDICINE_ID)
      references DBO.MEDICINE_INFO (MEDICINE_ID)
         on delete cascade
go

alter table DBO.PRESCRIPTION_DETAIL
   add constraint FK_P_D_R_M_I foreign key (MEDICINE_ID)
      references DBO.MEDICINE_INFO (MEDICINE_ID)
         on delete cascade
go

alter table DBO.PRESCRIPTION_DETAIL
   add constraint FK_P_D_R_P_I foreign key (PRESCRIPTION_ID)
      references DBO.PRESCRIPTION_INFO (PRESCRIPTION_ID)
         on delete cascade
go

alter table DBO.PRESCRIPTION_INFO
   add constraint FK_P_I_D_U_N foreign key (DEAL_USER_NAME)
      references DBO.USER_INFO (USER_NAME)
go

alter table DBO.PRESCRIPTION_INFO
   add constraint FK_P_I_R_C_U_I foreign key (CREATE_USER_NAME)
      references DBO.USER_INFO (USER_NAME)
go

alter table DBO.PRESCRIPTION_INFO_SPELL_INDEX
   add constraint FK_P_I_S_I_R_P_I foreign key (PRESCRIPTION_ID)
      references DBO.PRESCRIPTION_INFO (PRESCRIPTION_ID)
         on delete cascade
go

alter table DBO.STOCK_INFO
   add constraint FK_S_I_R_M_I foreign key (MEDICINE_ID)
      references DBO.MEDICINE_INFO (MEDICINE_ID)
         on delete cascade
go
