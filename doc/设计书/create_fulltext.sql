--打开数据库
use clinic
go
	--打开全文检索功能
	exec sp_fulltext_database @action = 'enable'
	--建立全文目录FT_CLINIC_MANAGE
	exec sp_fulltext_catalog @ftcat = 'FT_CLINIC_MANAGE', @action = 'create', @path = 'F:\Data\clinic-manage\db\FTData'
	
	--为MEDICINE_INFO表建立全文索引数据元
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO', @action = 'create', @ftcat = 'FT_CLINIC_MANAGE', @keyname = 'PK_MEDICINE_INFO'
	--设置全文索引列名
	exec sp_fulltext_column @tabname = 'MEDICINE_INFO', @colname = 'MEDICINE_NAME', @action = 'add', @language = 0x0804
	exec sp_fulltext_column @tabname = 'MEDICINE_INFO', @colname = 'MANUFACTURER', @action = 'add', @language = 0x0804
	--建立全文索引
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO', @action = 'activate'
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO', @action = 'start_change_tracking'
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO', @action = 'Start_background_updateindex'	
	
	--为PRESCRIPTION_INFO表建立全文索引数据元
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO', @action = 'create', @ftcat = 'FT_CLINIC_MANAGE', @keyname = 'PK_PRESCRIPTION_INFO'
	--设置全文索引列名
	exec sp_fulltext_column @tabname = 'PRESCRIPTION_INFO', @colname = 'PATIENT', @action = 'add', @language = 0x0804
	--建立全文索引
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO', @action = 'activate'
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO', @action = 'start_change_tracking'
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO', @action = 'Start_background_updateindex'	
	
	--为MEDICINE_INFO_SPELL_INDEX表建立全文索引数据元
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO_SPELL_INDEX', @action = 'create', @ftcat = 'FT_CLINIC_MANAGE', @keyname = 'PK_MEDICINE_INFO_SPELL_INDEX'
	--设置全文索引列名
	exec sp_fulltext_column @tabname = 'MEDICINE_INFO_SPELL_INDEX', @colname = 'MEDICINE_NAME_SPELL', @action = 'add', @language = 0x0409
	exec sp_fulltext_column @tabname = 'MEDICINE_INFO_SPELL_INDEX', @colname = 'MANUFACTURER_SPELL', @action = 'add', @language = 0x0409
	--建立全文索引
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO_SPELL_INDEX', @action = 'activate'
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO_SPELL_INDEX', @action = 'start_change_tracking'
	exec sp_fulltext_table @tabname = 'MEDICINE_INFO_SPELL_INDEX', @action = 'Start_background_updateindex'
	
	--为PRESCRIPTION_INFO_SPELL_INDEX表建立全文索引数据元
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO_SPELL_INDEX', @action = 'create', @ftcat = 'FT_CLINIC_MANAGE', @keyname = 'PK_PRESCRIPTION_INFO_SPELL_IND'
	--设置全文索引列名
	exec sp_fulltext_column @tabname = 'PRESCRIPTION_INFO_SPELL_INDEX', @colname = 'PATIENT_SPELL', @action = 'add', @language = 0x0409
	--建立全文索引
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO_SPELL_INDEX', @action = 'activate'
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO_SPELL_INDEX', @action = 'start_change_tracking'
	exec sp_fulltext_table @tabname = 'PRESCRIPTION_INFO_SPELL_INDEX', @action = 'Start_background_updateindex'	
		
	--填充全文索引目录
	exec sp_fulltext_catalog @ftcat = 'FT_CLINIC_MANAGE', @action = 'start_full'
		
go
	--检查全文目录填充情况
	WHILE FulltextCatalogProperty('FT_CLINIC_MANAGE','PopulateStatus')<>0
		BEGIN
			--如果全文目录正处于填充状态，则等待30秒后再检测一次
			WAITFOR DELAY '0:0:30'
		END
--全文目录填充完成后，使用全文目录检索

