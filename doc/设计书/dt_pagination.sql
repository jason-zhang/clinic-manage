CREATE PROCEDURE dt_pagination
@tbl_name   varchar(255),       -- 表名
@tbl_id varchar(255),					 --主键名
@column_list varchar(1000) = '*', -- 需要返回的列
@sort_list varchar(255)='',      -- 排序的字段名
@page_size   int = 10,          -- 页尺寸
@page_index int = 1,           -- 页码
@get_count bit = 0,   -- 返回记录总数, 非 0 值则返回
@order_type bit = 0, -- 设置排序类型, 非 0 值则降序
@str_where varchar(1500) = '' -- 查询条件 (注意: 不要加 where)
AS
declare @str_sql   varchar(5000)       -- 主语句
declare @str_tmp   varchar(110)        -- 临时变量
declare @str_order varchar(400)        -- 排序类型
if @get_count != 0
	begin
    if @str_where !=''
    set @str_sql = "select count(*) as total from [" + @tbl_name + "] where "+@str_where
    else
    set @str_sql = "select count(*) as total from [" + @tbl_name + "]"
	end
--以上代码的意思是如果@get_count传递过来的不是0，就执行总数统计。以下的所有代码都是@get_count为0的情况
else
	begin
		if @order_type != 0
			begin
			    set @str_tmp = "<(select min"
			    if @sort_list != ''
						set @str_order = " order by [" + @sort_list + "," + @tbl_id + "] desc"
					else
						set @str_order = " order by [" + @tbl_id + "] desc"
					--如果@order_type不是0，就执行降序，这句很重要！
			end
		else
			begin
		    set @str_tmp = ">(select max"
		    if @sort_list != ''
		    	set @str_order = " order by [" + @sort_list + "," + @tbl_id + "] asc"
		    else
		    	set @str_order = " order by [" + @tbl_id + "] asc"
			end
	if @page_index = 1
		begin
	    if @str_where != ''  
				set @str_sql = "select top " + str(@page_size) +" "+@column_list+ " from [" + @tbl_name + "] where " + @str_where + " " + @str_order
			else
				set @str_sql = "select top " + str(@page_size) +" "+@column_list+ " from ["+ @tbl_name + "] "+ @str_order
			--如果是第一页就执行以上代码，这样会加快执行速度
		end
	else
		begin
			--以下代码赋予了@str_sql以真正执行的SQL代码
			if @str_where != ''
				set @str_sql = "select top " + str(@page_size) +" "+@column_list+ " from ["
					+ @tbl_name + "] where [" + @tbl_id + "]" + @str_tmp + "(["
					+ @tbl_id + "]) from (select top " + str((@page_index-1)*@page_size) + " ["
					+ @tbl_id + "] from [" + @tbl_name + "] where " + @str_where + " "
					+ @str_order + ") as tbl_tmp) and " + @str_where + " " + @str_order
			else
				set @str_sql = "select top " + str(@page_size) +" "+@column_list+ " from ["
					+ @tbl_name + "] where [" + @tbl_id + "]" + @str_tmp + "(["
					+ @tbl_id + "]) from (select top " + str((@page_index-1)*@page_size) + " ["
					+ @tbl_id + "] from [" + @tbl_name + "]" + @str_order + ") as tbl_tmp)"+ @str_order			
		end
	end
exec (@str_sql)
GO