delete from USER_INFO;
insert into USER_INFO(USER_NAME,PASSWORD,USER_TYPE,ERROR_TIMES) values('admin','14e1b600b1fd579f47433b88e8d85291',0,0);
insert into USER_INFO(USER_NAME,PASSWORD,USER_TYPE,ERROR_TIMES) values('user','14e1b600b1fd579f47433b88e8d85291',1,0);
commit;


CREATE TRIGGER tr_medicine_info_insert
ON medicine_info
FOR INSERT
AS
DECLARE @m_name_spell varchar(1024),
   @manu_spell varchar(1024),
   @m_id int,
   @m_name nvarchar(128),
   @manu nvarchar(128),
   @n_len  int,
   @i int,
   @ch nvarchar(2),
   @spell varchar(64)
DECLARE medicine_cursor CURSOR FOR
SELECT medicine_id,medicine_name,manufacturer FROM inserted

OPEN medicine_cursor

FETCH NEXT FROM medicine_cursor
INTO @m_id, @m_name, @manu

WHILE @@FETCH_STATUS = 0
BEGIN
   --生成药品名称索引
   set @m_name_spell=''
   set @n_len=len(@m_name)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@m_name,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @m_name_spell=@m_name_spell+@spell
     else
       set @m_name_spell=@m_name_spell+@ch
     set @m_name_spell=@m_name_spell+' '
     set @i=@i+1
    END
   
   --生成生产厂商索引
   set @manu_spell=''
   set @n_len=len(@manu)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@manu,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @manu_spell=@manu_spell+@spell
     else
       set @manu_spell=@manu_spell+@ch
     set @manu_spell=@manu_spell+' '
     set @i=@i+1
    END

   insert into medicine_info_spell_index(medicine_id,medicine_name_spell,manufacturer_spell)
   values(@m_id,@m_name_spell,@manu_spell)
   FETCH NEXT FROM medicine_cursor
   INTO @m_id, @m_name, @manu
END

CLOSE medicine_cursor
DEALLOCATE medicine_cursor


CREATE TRIGGER tr_medicine_info_update
ON medicine_info
FOR UPDATE
AS
DECLARE @m_name_spell varchar(1024),
   @manu_spell varchar(1024),
   @m_id int,
   @m_name nvarchar(128),
   @manu nvarchar(128),
   @n_len  int,
   @i int,
   @ch nvarchar(2),
   @spell varchar(64)
DECLARE medicine_cursor CURSOR FOR
SELECT medicine_id,medicine_name,manufacturer FROM inserted

OPEN medicine_cursor

FETCH NEXT FROM medicine_cursor
INTO @m_id, @m_name, @manu

WHILE @@FETCH_STATUS = 0
BEGIN
   --生成药品名称索引
   set @m_name_spell=''
   set @n_len=len(@m_name)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@m_name,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @m_name_spell=@m_name_spell+@spell
     else
       set @m_name_spell=@m_name_spell+@ch
     set @m_name_spell=@m_name_spell+' '
     set @i=@i+1
    END
   
   --生成生产厂商索引
   set @manu_spell=''
   set @n_len=len(@manu)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@manu,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @manu_spell=@manu_spell+@spell
     else
       set @manu_spell=@manu_spell+@ch
     set @manu_spell=@manu_spell+' '
     set @i=@i+1
    END

   update medicine_info_spell_index set medicine_name_spell=@m_name_spell,manufacturer_spell=@manu_spell where medicine_id=@m_id
   FETCH NEXT FROM medicine_cursor
   INTO @m_id, @m_name, @manu
END

CLOSE medicine_cursor
DEALLOCATE medicine_cursor


CREATE TRIGGER tr_medicine_info_delete
ON medicine_info
FOR DELETE
AS
DECLARE @m_id int
DECLARE medicine_cursor CURSOR FOR
SELECT medicine_id FROM deleted

OPEN medicine_cursor

FETCH NEXT FROM medicine_cursor
INTO @m_id

WHILE @@FETCH_STATUS = 0
BEGIN
   delete from medicine_info_spell_index where medicine_id=@m_id
   FETCH NEXT FROM medicine_cursor
   INTO @m_id
END

CLOSE medicine_cursor
DEALLOCATE medicine_cursor


CREATE TRIGGER tr_prescription_info_insert
ON prescription_info
FOR INSERT
AS
DECLARE @patient_spell varchar(1024),
   @p_id int,
   @patient nvarchar(50),
   @n_len  int,
   @i int,
   @ch nvarchar(2),
   @spell varchar(64)
DECLARE prescription_cursor CURSOR FOR
SELECT prescription_id,patient FROM inserted

OPEN prescription_cursor

FETCH NEXT FROM prescription_cursor
INTO @p_id, @patient

WHILE @@FETCH_STATUS = 0
BEGIN
   --生成病人姓名索引
   set @patient_spell=''
   set @n_len=len(@patient)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@patient,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @patient_spell=@patient_spell+@spell
     else
       set @patient_spell=@patient_spell+@ch
     set @patient_spell=@patient_spell+' '
     set @i=@i+1
    END

   insert into prescription_info_spell_index(prescription_id,patient_spell)
   values(@p_id,@patient_spell)
   FETCH NEXT FROM prescription_cursor
   INTO @p_id, @patient
END

CLOSE prescription_cursor
DEALLOCATE prescription_cursor


CREATE TRIGGER tr_prescription_info_update
ON prescription_info
FOR UPDATE
AS
DECLARE @patient_spell varchar(1024),
   @p_id int,
   @patient nvarchar(50),
   @n_len  int,
   @i int,
   @ch nvarchar(2),
   @spell varchar(64)
DECLARE prescription_cursor CURSOR FOR
SELECT prescription_id,patient FROM inserted

OPEN prescription_cursor

FETCH NEXT FROM prescription_cursor
INTO @p_id, @patient

WHILE @@FETCH_STATUS = 0
BEGIN
   --生成病人姓名索引
   set @patient_spell=''
   set @n_len=len(@patient)
   set @i=0
   WHILE @i<@n_len
    BEGIN
     set @ch=SUBSTRING(@patient,@i+1,1)
     set @spell=''
     select @spell=spell from spell where chinese_character=@ch
     if len(@spell)>0
       set @patient_spell=@patient_spell+@spell
     else
       set @patient_spell=@patient_spell+@ch
     set @patient_spell=@patient_spell+' '
     set @i=@i+1
    END

   update prescription_info_spell_index set patient_spell=@patient_spell where prescription_id=@p_id
   FETCH NEXT FROM prescription_cursor
   INTO @p_id, @patient
END

CLOSE prescription_cursor
DEALLOCATE prescription_cursor


CREATE TRIGGER tr_prescription_info_delete
ON prescription_info
FOR DELETE
AS
DECLARE @p_id int
DECLARE prescription_cursor CURSOR FOR
SELECT prescription_id FROM deleted

OPEN prescription_cursor

FETCH NEXT FROM prescription_cursor
INTO @p_id

WHILE @@FETCH_STATUS = 0
BEGIN
   delete from prescription_info_spell_index where prescription_id=@p_id
   FETCH NEXT FROM prescription_cursor
   INTO @p_id
END

CLOSE prescription_cursor
DEALLOCATE prescription_cursor

