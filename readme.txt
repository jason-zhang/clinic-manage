目录结构介绍

1.src
 存放系统源代码

2.test
 存放测试代码

3.lib
 存放系统编译成exec文件后需要的运行库文件，pb9下是Power Builder 9的运行库，sql sever 2000下存放ms sql server 2000客户端连接运行库

4.doc
 存放系统文档

5.参考资料
 存放开发系统需要的资料，其中google code里面是关于google code使用方法的文章介绍，开特人力资源管理系统是我以前做的毕业实习的一个系统，文档模板是我们公司的一些文档模板
