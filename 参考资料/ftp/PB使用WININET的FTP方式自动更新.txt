使用%windir%/system32/wininet.dll来实现
原谅地址
http://blog.csdn.net/handycyw/MyArticles.aspx


使用PB调用API自动更新（非FTP模式）（一、STRUCTURE定义）
一、STRUCTURE定义
1、s_filetime
global type s_filetime from structure
 long  dwlowdatetime
 long  dwhighdatetime
end type

2、s_win32_find_data
global type s_win32_find_data from structure
 long  dwfileattributes
 s_filetime  ftcreationtime
 s_filetime  ftlastaccesstime
 s_filetime  ftlastwritetime
 long  nfilesizehigh
 long  nfilesizelow
 long  dwreserved0
 long  dwreserved1
 character  cfilename[255]
 character  calternatefilename[14]
end type

3、s_netresource
global type s_netresource from structure
 long  dwScope
 long  dwType
 long  dwDisplayType
 long  dwUsage
 string  lpLocalName
 string  lpRemoteName
 string  lpComment
 string  lpProvider
end type


PB使用WININET的FTP方式自动更新（一、API和全局变量）
一些结构和API在非FTP方式文档中有了定义就不再说明。
一、API声明
function uLong InternetOpenA(string lpszAgent, ulong dwAccessType,string lpszProxyName,string lpszProxyBypass, ulong dwFlags) library "wininet.dll"
function ulong InternetConnectA(ulong hInternet,string lpszServerName,ulong nServerPort,string lpszUsername,string lpszPassword,ulong dwService,ulong dwFlags,ulong dwContext) library "wininet.dll"
function boolean InternetCloseHandle(ulong hInternet) library "wininet.dll"
function boolean FtpSetCurrentDirectoryA(ulong hConnect,string lpszDirectory) library "wininet.dll"
function ulong FtpFindFirstFileA(ulong hConnect,string lpszSearchFile,ref s_WIN32_FIND_DATA lpFindFileData,ulong dwFlags,ulong dwContext) library "wininet.dll"
function boolean InternetFindNextFileA(ulong hFind,ref s_WIN32_FIND_DATA lpvFindData) library "wininet.dll"
function boolean FtpGetFileA(ulong hConnect, string lpszRemoteFile,string lpszNewFile,boolean fFailIfExists,ulong dwFlagsAndAttributes,ulong dwFlags,ulong dwContext) library "wininet.dll"
二、全局变量
string gs_application_name
STRING GS_FTPSERVER1
STRING GS_FTPSERVER2
STRING GS_FTPSERVER3
STRING GS_FTPLOGINUSER1
STRING GS_FTPLOGINUSER2
STRING GS_FTPLOGINUSER3
STRING GS_FTPLOGINPASS1
STRING GS_FTPLOGINPASS2
STRING GS_FTPLOGINPASS3
STRING GS_FTPLOGINPORT1
STRING GS_FTPLOGINPORT2
STRING GS_FTPLOGINPORT3
LONG   GL_FTPLOGINPORT1
LONG   GL_FTPLOGINPORT2
LONG   GL_FTPLOGINPORT3
STRING GS_FTPDIR1
STRING GS_FTPDIR2
STRING GS_FTPDIR3

GS_FTPSERVER1    = ProfileString("autodown.ini","downpath","FTPSERVER1","")
GS_FTPSERVER2    = ProfileString("autodown.ini","downpath","FTPSERVER2","")
GS_FTPSERVER3    = ProfileString("autodown.ini","downpath","FTPSERVER3","")
GS_FTPLOGINUSER1 = ProfileString("autodown.ini","downpath","FTPLOGINUSER1","")
GS_FTPLOGINUSER2 = ProfileString("autodown.ini","downpath","FTPLOGINUSER2","")
GS_FTPLOGINUSER3 = ProfileString("autodown.ini","downpath","FTPLOGINUSER3","")
GS_FTPLOGINPASS1 = ProfileString("autodown.ini","downpath","FTPLOGINPASS1","")
GS_FTPLOGINPASS2 = ProfileString("autodown.ini","downpath","FTPLOGINPASS2","")
GS_FTPLOGINPASS3 = ProfileString("autodown.ini","downpath","FTPLOGINPASS3","")
GL_FTPLOGINPORT1 = ProfileInt("autodown.ini","downpath","FTPLOGINPORT1",21)
GL_FTPLOGINPORT2 = ProfileInt("autodown.ini","downpath","FTPLOGINPORT2",21)
GL_FTPLOGINPORT3 = ProfileInt("autodown.ini","downpath","FTPLOGINPORT3",21)
GS_FTPDIR1        = ProfileString("autodown.ini","downpath","FTPDIR1","")
GS_FTPDIR2        = ProfileString("autodown.ini","downpath","FTPDIR2","")
GS_FTPDIR3        = ProfileString("autodown.ini","downpath","FTPDIR3","")


PB使用WININET的FTP方式自动更新（二、判断是否可以连接）
global type f_connect_ftpserver from function_object
end type

forward prototypes
global function integer f_connect_ftpserver ()
end prototypes

global function integer f_connect_ftpserver ();
long ll_hret
long ll_hftp
integer li_ret = -1

do while yield()
loop

ll_hret = InternetOpenA(gs_application_name,0,'','',0)
if ll_hret = 0 or isnull(ll_hret) then
  //messagebox('错误！','缺少动态连接库WinInet.Dll！',stopsign!)
 return li_ret
end if
ll_hftp = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
if ll_hftp = 0 then
  ll_hftp = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
 if ll_hftp = 0 then
  ll_hftp = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
  if ll_hftp = 0 then
   InternetCloseHandle(ll_hret)
   return li_ret
  ELSE
   li_ret = 3
  END IF
 ELSE
  li_ret = 2
 END IF
else
 li_ret = 1
end if
InternetCloseHandle(ll_hret)
InternetCloseHandle(ll_hftp)
return li_ret

end function


PB使用WININET的FTP方式自动更新（三、获得要更新的文件数）
三、获得要更新的文件数
（只是处理二级目录）
global type f_connect_ftpcount from function_object
end type

forward prototypes
global function integer f_connect_ftpcount ()
end prototypes

global function integer f_connect_ftpcount ();
do while yield()
loop

string ls_directorya1,ls_directorya2

ls_directorya2 = gs_application_path

string ls_directoryb1,ls_directoryb2
string ls_filename1,ls_filename2

long ll_hret
long ll_hftp1
long ll_hftp2
long ll_fftp1
long ll_fftp2
integer li_ret
long ll_filehandle1,ll_filehandle2
long ll_k

boolean lb_success

s_WIN32_FIND_DATA ss_file1,ss_file2

li_ret = f_connect_ftpserver()
if li_ret = -1 then return -1

ll_hret = InternetOpenA(gs_application_name,0,'','',0)
choose case li_ret
 case 1
  ls_directorya1 = GS_FTPDIR1
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
 case 2
  ls_directorya1 = GS_FTPDIR2
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
 case 3
  ls_directorya1 = GS_FTPDIR3
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
 case else
  InternetCloseHandle(ll_hret);
  return -1
end choose
lb_success = FtpSetCurrentDirectoryA(ll_hftp1,ls_directorya1)
if not lb_success then
 InternetCloseHandle(ll_hret);
 InternetCloseHandle(ll_hftp1);
 return -1
end IF
ll_fftp1 = FtpFindFirstFileA(ll_hftp1,'',ss_file1,0,0)
if ll_fftp1 = 0 then
 InternetCloseHandle(ll_hret);
 InternetCloseHandle(ll_hftp1);
 InternetCloseHandle(ll_fftp1);
 return 0
end if
ll_k = 0
if ss_file1.dwFileAttributes = 16 then
 ls_directoryb1 = ls_directorya1 + '/' + ss_file1.cfilename
 ls_directoryb2 = ls_directorya2 + '\' + ss_file1.cfilename
 if directoryexists(ls_directoryb2) = false then
  ll_k++
 end if
 choose case li_ret
  case 1
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
  case 2
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
  case 3
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
 end choose
 lb_success = FtpSetCurrentDirectoryA(ll_hftp2,ls_directoryb1)
 if lb_success  then
  ll_fftp2 = FtpFindFirstFileA(ll_hftp2,'',ss_file1,0,0)
  ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
  if fileexists(ls_filename2) = false then
   ll_k++
  else
   ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
   FindClose(ll_filehandle2)
   choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
    case 0
     //两个时间相等，就返回零
    case 1
     //如lpFileTime2小于lpFileTime1，返回1
     //下载
     ll_k++
    case -1
     //如lpFileTime1小于lpFileTime2，返回-1
   end choose
  end if
  do while InternetFindnextFileA(ll_fftp2, ss_file1)
   yield()
   ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
   if fileexists(ls_filename2) = false then
    ll_k++
   else
    ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
    FindClose(ll_filehandle2)
    choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
     case 0
      //两个时间相等，就返回零
     case 1
      //如lpFileTime2小于lpFileTime1，返回1
      //下载
      ll_k++
     case -1
      //如lpFileTime1小于lpFileTime2，返回-1
    end choose
   end if
  loop
  InternetCloseHandle(ll_hftp2);
  InternetCloseHandle(ll_fftp2);
 end if
else
 ls_filename2 = ls_directorya2 + '\' + ss_file1.cfilename
 if fileexists(ls_filename2) = false then
  ll_k++
 else
  ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
  FindClose(ll_filehandle2)
  choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
   case 0
    //两个时间相等，就返回零
   case 1
    //如lpFileTime2小于lpFileTime1，返回1
    //下载
    ll_k++
   case -1
    //如lpFileTime1小于lpFileTime2，返回-1
  end choose
 end if
end if
do while InternetFindnextFileA(ll_fftp1, ss_file1)
 yield()
 if ss_file1.dwFileAttributes = 16 then
  ls_directoryb1 = ls_directorya1 + '/' + ss_file1.cfilename
  ls_directoryb2 = ls_directorya2 + '\' + ss_file1.cfilename
  if directoryexists(ls_directoryb2) = false then
   ll_k++
  end if
  choose case li_ret
   case 1
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
   case 2
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
   case 3
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
  end choose
  lb_success = FtpSetCurrentDirectoryA(ll_hftp2,ls_directoryb1)
  if lb_success then
   ll_fftp2 = FtpFindFirstFileA(ll_hftp2,'',ss_file1,0,0)
   ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
   if fileexists(ls_filename2) = false then
    ll_k++
   else
    ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
    FindClose(ll_filehandle2)
    choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
     case 0
      //两个时间相等，就返回零
     case 1
      //如lpFileTime2小于lpFileTime1，返回1
      //下载
      ll_k++
     case -1
      //如lpFileTime1小于lpFileTime2，返回-1
    end choose
   end if
   do while InternetFindnextFileA(ll_fftp2, ss_file1)
    yield()
    ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
    if fileexists(ls_filename2) = false then
     ll_k++
    else
     ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
     FindClose(ll_filehandle2)
     choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
      case 0
       //两个时间相等，就返回零
      case 1
       //如lpFileTime2小于lpFileTime1，返回1
       //下载
       ll_k++
      case -1
       //如lpFileTime1小于lpFileTime2，返回-1
     end choose
    end if
   loop
   InternetCloseHandle(ll_hftp2);
   InternetCloseHandle(ll_fftp2);
  end if
 else
  ls_filename2 = ls_directorya2 + '\' + ss_file1.cfilename
  if fileexists(ls_filename2) = false then
   ll_k++
  else
   ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
   FindClose(ll_filehandle2)
   choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
    case 0
     //两个时间相等，就返回零
    case 1
     //如lpFileTime2小于lpFileTime1，返回1
     //下载
     ll_k++
    case -1
     //如lpFileTime1小于lpFileTime2，返回-1
   end choose
  end if
 end if
loop
InternetCloseHandle(ll_hret);
InternetCloseHandle(ll_hftp1);
InternetCloseHandle(ll_fftp1);
return ll_k

end function


PB使用WININET的FTP方式自动更新（四、下载）
四、下载
(只是处理二级目录)
global type f_connect_ftpcopyfile from function_object
end type

forward prototypes
global function integer f_connect_ftpcopyfile (hprogressbar hpb_1, long al_copycount)
end prototypes

global function integer f_connect_ftpcopyfile (hprogressbar hpb_1, long al_copycount);
do while yield()
loop

string ls_directorya1 //远程目录
string ls_directorya2 //本地目录
string ls_filename,ls_file

//本地目录
ls_directorya2 = gs_application_path

string ls_directoryb1 //远程目录
string ls_directoryb2 //本地目录

string ls_filename1 //远程文件
string ls_filename2 //本地文件

long ll_hret
long ll_hftp1
long ll_hftp2
long ll_fftp1
long ll_fftp2
long ll_m
long ll_filehandle1,ll_filehandle2

integer li_ret

boolean lb_success
boolean lb_find

s_WIN32_FIND_DATA ss_file1,ss_file2

w_autodown_test.st_ts.text = '正在检查需要更新软件的数量...'
li_ret = f_connect_ftpserver()
if li_ret = -1 then return -1
w_autodown_test.st_ts.text = '正在更新软件版本...'
//建立连接
ll_hret = InternetOpenA(gs_application_name,0,'','',0)
choose case li_ret
 case 1
  ls_directorya1 = GS_FTPDIR1
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
 case 2
  ls_directorya1 = GS_FTPDIR2
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
 case 3
  ls_directorya1 = GS_FTPDIR3
  ll_hftp1 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
 case else
  InternetCloseHandle(ll_hret);
  return -1
end choose
//进入FTP目录
lb_success = FtpSetCurrentDirectoryA(ll_hftp1,ls_directorya1)
if not lb_success then
 InternetCloseHandle(ll_hret);
 InternetCloseHandle(ll_hftp1);
 return -1
end IF
//获取第一个文件
ll_fftp1 = FtpFindFirstFileA(ll_hftp1,'',ss_file1,0,0)
if ll_fftp1 = 0 then
 InternetCloseHandle(ll_hret);
 InternetCloseHandle(ll_hftp1);
 InternetCloseHandle(ll_fftp1);
 return 0
end if
//初始化提示栏
hpb_1.position = 0
ll_m = 0
//是目录
if ss_file1.dwFileAttributes = 16 then
 //远程目录
 ls_directoryb1 = ls_directorya1 + '/' + ss_file1.cfilename
 //本地目录
 ls_directoryb2 = ls_directorya2 + '\' + ss_file1.cfilename
 //本地目录不存在的创建目录
 if directoryexists(ls_directoryb2) = false then
  CreateDirectory(ls_directoryb2)
  ll_m++
  hpb_1.position = ll_m / al_copycount * 100
 end if
 //创建第二个目录句柄
 choose case li_ret
  case 1
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
  case 2
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
  case 3
   ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
 end choose
 //转移到第二级目录
 lb_success = FtpSetCurrentDirectoryA(ll_hftp2,ls_directoryb1)
 if lb_success  then
  //浏览目录内容
  ll_fftp2 = FtpFindFirstFileA(ll_hftp2,'',ss_file1,0,0)
  //本地文件
  ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
  if fileexists(ls_filename2) = false then
   //本地文件不存在的，直接获取
   FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
   ll_m++
   hpb_1.position = ll_m / al_copycount * 100
  else //本地文件存在的，比较文件时间
   ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
   FindClose(ll_filehandle2)
   choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
    case 0
     //两个时间相等，就返回零
    case 1
     //如lpFileTime2小于lpFileTime1，返回1
     //下载
     FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
     ll_m++
     hpb_1.position = ll_m / al_copycount * 100
    case -1
     //如lpFileTime1小于lpFileTime2，返回-1
   end choose
  end if
  //遍历目录
  do while InternetFindnextFileA(ll_fftp2, ss_file1)
   yield()
   //本地文件
   ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
   if fileexists(ls_filename2) = false then
    //本地文件不存在的，直接获取
    FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
    ll_m++
    hpb_1.position = ll_m / al_copycount * 100
   else //本地文件存在的，比较文件时间
    ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
    FindClose(ll_filehandle2)
    choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
     case 0
      //两个时间相等，就返回零
     case 1
      //如lpFileTime2小于lpFileTime1，返回1
      //下载
      FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
      ll_m++
      hpb_1.position = ll_m / al_copycount * 100
     case -1
      //如lpFileTime1小于lpFileTime2，返回-1
    end choose
   end if
  loop
  InternetCloseHandle(ll_hftp2);
  InternetCloseHandle(ll_fftp2);
 end if
else //是文件
 //本地文件
 ls_filename2 = ls_directorya2 + '\' + ss_file1.cfilename
 if fileexists(ls_filename2) = false then
  //本地文件不存在的，直接获取
  lb_find = FtpGetFileA(ll_hftp1,ss_file1.cfilename,ls_filename2,false,0,0,0)
  ll_m++
  hpb_1.position = ll_m / al_copycount * 100
 else //本地文件存在的，比较文件时间
  ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
  FindClose(ll_filehandle2)
  choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
   case 0
    //两个时间相等，就返回零
   case 1
    //如lpFileTime2小于lpFileTime1，返回1
    //下载
    lb_find = FtpGetFileA(ll_hftp1,ss_file1.cfilename,ls_filename2,false,0,0,0)
    ll_m++
    hpb_1.position = ll_m / al_copycount * 100
   case -1
    //如lpFileTime1小于lpFileTime2，返回-1
  end choose
 end if
end if
//遍历目录
do while InternetFindnextFileA(ll_fftp1, ss_file1)
 yield()
 //是目录
 if ss_file1.dwFileAttributes = 16 then
  //远程目录
  ls_directoryb1 = ls_directorya1 + '/' + ss_file1.cfilename
  //本地目录
  ls_directoryb2 = ls_directorya2 + '\' + ss_file1.cfilename
  //本地目录不存在的创建目录
  if directoryexists(ls_directoryb2) = false then
   CreateDirectory(ls_directoryb2)
   ll_m++
   hpb_1.position = ll_m / al_copycount * 100
  end if
  //创建第二个目录句柄
  choose case li_ret
   case 1
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER1,GL_FTPLOGINPORT1,GS_FTPLOGINUSER1,GS_FTPLOGINPASS1,1,0,0)
   case 2
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER2,GL_FTPLOGINPORT2,GS_FTPLOGINUSER2,GS_FTPLOGINPASS2,1,0,0)
   case 3
    ll_hftp2 = InternetConnectA(ll_hret,GS_FTPSERVER3,GL_FTPLOGINPORT3,GS_FTPLOGINUSER3,GS_FTPLOGINPASS3,1,0,0)
  end choose
  //转移到第二级目录
  lb_success = FtpSetCurrentDirectoryA(ll_hftp2,ls_directoryb1)
  if lb_success then
   //浏览目录内容
   ll_fftp2 = FtpFindFirstFileA(ll_hftp2,'',ss_file1,0,0)
   //本地文件
   ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
   if fileexists(ls_filename2) = false then
    //本地文件不存在的，直接获取
    lb_find = FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
    ll_m++
    hpb_1.position = ll_m / al_copycount * 100
   else //本地文件存在的，比较文件时间
    ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
    FindClose(ll_filehandle2)
    choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
     case 0
      //两个时间相等，就返回零
     case 1
      //如lpFileTime2小于lpFileTime1，返回1
      //下载
      lb_find = FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
      ll_m++
      hpb_1.position = ll_m / al_copycount * 100
     case -1
      //如lpFileTime1小于lpFileTime2，返回-1
    end choose
   end if
   //遍历目录
   do while InternetFindnextFileA(ll_fftp2, ss_file1)
    yield()
    //本地文件
    ls_filename2 = ls_directoryb2 + '\' + ss_file1.cfilename
    if fileexists(ls_filename2) = false then
     //本地文件不存在的，直接获取
     lb_find = FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
     ll_m++
     hpb_1.position = ll_m / al_copycount * 100
    else //本地文件存在的，比较文件时间
     ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
     FindClose(ll_filehandle2)
     choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
      case 0
       //两个时间相等，就返回零
      case 1
       //如lpFileTime2小于lpFileTime1，返回1
       //下载
       lb_find = FtpGetFileA(ll_hftp2,ss_file1.cfilename,ls_filename2,false,0,0,0)
       ll_m++
       hpb_1.position = ll_m / al_copycount * 100
      case -1
       //如lpFileTime1小于lpFileTime2，返回-1
     end choose
    end if
   loop
   InternetCloseHandle(ll_hftp2);
   InternetCloseHandle(ll_fftp2);
  end if
 else //是文件
  //本地文件
  ls_filename2 = ls_directorya2 + '\' + ss_file1.cfilename
  if fileexists(ls_filename2) = false then
   //本地文件不存在的，直接获取
   lb_find = FtpGetFileA(ll_hftp1,ss_file1.cfilename,ls_filename2,false,0,0,0)
   ll_m++
   hpb_1.position = ll_m / al_copycount * 100
  else //本地文件存在的，比较文件时间
   ll_filehandle2 = FindFirstFileA(ls_filename2,ss_file2)
   FindClose(ll_filehandle2)
   choose case CompareFileTime(ss_file1.ftlastwritetime,ss_file2.ftlastwritetime)
    case 0
     //两个时间相等，就返回零
    case 1
     //如lpFileTime2小于lpFileTime1，返回1
     //下载
     lb_find = FtpGetFileA(ll_hftp1,ss_file1.cfilename,ls_filename2,false,0,0,0)
     ll_m++
     hpb_1.position = ll_m / al_copycount * 100
    case -1
     //如lpFileTime1小于lpFileTime2，返回-1
   end choose
  end if
 end if
loop
InternetCloseHandle(ll_hret);
InternetCloseHandle(ll_hftp1);
InternetCloseHandle(ll_fftp1);
return 1

end function

使用PB调用API自动更新（非FTP模式）（六、AUTODOWN.INI文件）
六、AUTODOWN.INI文件
[downpath]
COPYSERVER1=\\dl-server\hbky_update
COPYSERVER2=\\dl-server\hbky_update
COPYSERVER3=
COPYLOGINUSER1=administrator
COPYLOGINUSER2=hbkyautodown
COPYLOGINUSER3=
COPYLOGINPASS1=gygsglk
COPYLOGINPASS2=hbkyautodown
COPYLOGINPASS3=