[Setup]
AppName=Association Miner
AppVerName=Association Miner V 1.0.0.0
AppPublisher=Hust Miner
AppVersion=1.0.0.0
VersionInfoVersion=1.0.0.0
VersionInfoTextVersion=1.0.0.0
DefaultDirName={pf}\Apriori
DefaultGroupName=Association Miner
UninstallDisplayName=Association Miner V1.0.0.0
UninstallDisplayIcon={app}\MyProg.exe
AppCopyright=Copyrihgt@2005 Hust Miner All rights reserved.
Compression=lzma/max
SolidCompression=yes
WindowVisible=yes
WizardImageFile=pic\WizardImageFile.bmp
WizardSmallImageFile=pic\WizModernSmallImage.bmp

[Files]
Source: "exec\Apriori.exe"; DestDir: "{app}"
Source: "exec\Data\*.*"; DestDir: "{app}\Data"
Source: "exec\SYS\*.*"; DestDir: "{sys}"; Flags : restartreplace uninsneveruninstall sharedfile
Source: "exec\SVR\*.*"; DestDir: "{sys}"; Flags : restartreplace uninsneveruninstall sharedfile regserver

[Icons]
Name: "{group}\Association Miner"; Filename: "{app}\Apriori.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall Association Miner"; Filename: "{uninstallexe}"; WorkingDir: "{app}"; IconFilename: "{win}\explorer.exe"; IconIndex: 6
Name: "{userdesktop}\Association Miner"; Filename: "{app}\Apriori.exe"; WorkingDir: "{app}"

[Registry]
Root: HKCU; Subkey: "Software\Hust Miner"; Flags: uninsdeletekeyifempty
Root: HKCU; Subkey: "Software\Hust Miner\Association Miner"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Hust Miner"; Flags: uninsdeletekeyifempty
Root: HKLM; Subkey: "Software\Hust Miner\Association Miner"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Hust Miner\Association Miner\Settings"; ValueType: string; ValueName: "Path"; ValueData: "{app}"
Root: HKLM; Subkey: "Software\Hust Miner\Association Miner\Settings"; ValueType: string; ValueName: "Version"; ValueData: "1.0.0"

[Run]
Filename: "{app}\Apriori.exe"; Description: "Launch program"; Flags: postinstall nowait skipifsilent

[UninstallDelete]
Type: filesandordirs; Name: "{app}"

[Languages]
Name: "en"; MessagesFile: "compiler:\Languages\English.isl"
Name: "cn"; MessagesFile: "compiler:Default.isl"
